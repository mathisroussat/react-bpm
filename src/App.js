import React, { useState, useEffect } from 'react';

const BluetoothComponent = () => {
  const [devices, setDevices] = useState([]);
  const [selectedDevice, setSelectedDevice] = useState(null);
  const [characteristicValue, setCharacteristicValue] = useState(null);
  const [connecting, setConnecting] = useState(false);

  const discoverDevices = async () => {
    try {
      // Demander à l'utilisateur l'autorisation d'activer le Bluetooth
      const device = await navigator.bluetooth.requestDevice({
        acceptAllDevices: true,
      });

      // Mettre à jour la liste des périphériques découverts
      setDevices((prevDevices) => [...prevDevices, device]);
    } catch (error) {
      console.error('Erreur lors de la découverte des périphériques:', error);
    }
  };

  const connectToDevice = async () => {
    try {
      // Connecter le périphérique sélectionné
      const server = await selectedDevice.gatt.connect();

      // Obtenir le service avec l'UUID spécifié
      const service = await server.getPrimaryService('0000180f-0000-1000-8000-00805f9b34fb');

      // Obtenir les caractéristiques du service
      const characteristics = await service.getCharacteristics();

      // Trouver la caractéristique avec l'UUID spécifié
      const targetCharacteristic = characteristics.find(
        (char) => char.uuid === '0000180f-0000-1000-8000-00805f9b34fb'
      );

      if (targetCharacteristic) {
        // Lire la valeur de la caractéristique
        const value = await targetCharacteristic.readValue();
        setCharacteristicValue(value);
      } else {
        throw new Error('Caractéristique non trouvée');
      }
    } catch (error) {
      console.error('Erreur lors de la connexion au périphérique:', error);
    } finally {
      setConnecting(false);
    }
  };

  return (
    <div>
      {selectedDevice ? (
        <div>
          <h2>Périphérique connecté</h2>
          <p>Nom: {selectedDevice.name}</p>
          <p>UUID: {selectedDevice.id}</p>
          {characteristicValue && (
            <p>Caractéristique valeur: {characteristicValue.buffer ? characteristicValue.buffer : characteristicValue}</p>
          )}
        </div>
      ) : (
        <div>
          <p>En attente de la connexion au périphérique Bluetooth...</p>
          <button onClick={() => discoverDevices()} disabled={connecting}>
            Découvrir les périphériques Bluetooth
          </button>

          <ul>
            {devices.map((device, index) => (
              <li key={index}>
                <button onClick={() => setSelectedDevice(device)}>Connecter à {device.name}</button>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default BluetoothComponent;
